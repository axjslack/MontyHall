#include <stdio.h>
#include <stdlib.h> 
#include <time.h> 




#ifdef THR
// Pthread includes
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#define NUM_THREADS 24

static pthread_mutex_t win_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t played_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

long long int i, nplay=10000000, played=0;
static long long int nwin=0;


int get_choice()
{
	int rndn;

	while(1)
	{
		rndn=rand();
		rndn=0x7 & rndn;
		if( rndn == 1 || rndn == 2 || rndn == 4 )
		{
			return rndn;
		}
	}

}

int open_door(int v1,int p1)
{
	int m1=0;

	if( v1 == p1 )
	{
		while(m1 == 0)
		{
			m1 = get_choice();
			if(m1 == p1) {m1=0;}
		}
	}
	else{
	m1= 0x7 & ~( p1 | v1 );}


/*
	switch(v1){
	case 1:
		if( p1 == 1 ) {  }



		case 1: m1 = ( p1 == 2) ? 4 : 2; break;	
		case 2: m1 = ( p1 == 1) ? 4 : 1; break;	
		case 4: m1 = ( p1 == 2) ? 2 : 1; break;	
	}*/



	return m1;	


}



int gamerun()

{
	int v1, p1, m1, f1;

	v1=get_choice();
	p1=get_choice();
	m1=open_door(v1,p1);
	f1=open_door(m1,p1);
	played++;
	//printf ("First choice %d, winning %d, opened %d, final choice %d ",p1,v1,m1,f1);
	if(v1 == f1 ) {
		//printf("Winner\n");
		return 1; }
	else {
		//printf("Loser\n");
		return 0; }

}

#ifdef THR
void *gamerun_thr(void *nothing)
{


	int v1, p1, m1, f1;
	long long int i;
	pthread_t tid;

	tid=pthread_self();

	for(i=0;i<nplay;i++)  
	{
		v1=0;
		p1=0;
		m1=0;
		f1=0; 

		v1=get_choice();
		p1=get_choice();
		m1=open_door(v1,p1);
		f1=open_door(m1,p1);
	
		
		//printf ("Thread: %d First choice %d, winning %d, opened %d, final choice %d \n", (int)tid, p1,v1,m1,f1);
		pthread_mutex_lock(&played_mutex);		
		played++;
		pthread_mutex_unlock(&played_mutex);	
		if(v1 == f1 ) 
		{
			//printf("Winner\n");
			pthread_mutex_lock(&win_mutex);
			nwin++;
			pthread_mutex_unlock(&win_mutex);
		}
		
	}
	
	pthread_exit(NULL);	
}

#endif

double percent(long long int played, long long int win)
{
	double temp;

	temp=(double) (win /(played/100));
	return temp;

}



int main(int argc, char *argv[])
{


#ifdef THR
	int t,rc;
	
	pthread_t threads[NUM_THREADS];
#endif
	srand(time(0));
	
	printf("Rand MAX is equal to: %d\n", RAND_MAX);

#ifdef THR	
	for (t=0; t<NUM_THREADS; t++) {
		printf("Creating thread n %d\n",t);
		//sleep(1);
		rc=pthread_create(&threads[t], NULL, gamerun_thr, NULL);
		if(rc!=0) { printf("Error on thread creation: %d\n ",rc);}
	}

	for (t=0; t<NUM_THREADS; t++) 
	{
		pthread_join(threads[t],NULL);
	}
	sleep(1);

#else
	for(i=0;i<nplay;i++)
	{
		nwin+=gamerun();//printf("%d \n", get_choice());
	}

#endif 
	printf("Total Played: %llu, win: %llu\n, percentage: %f", played, nwin, (float)percent(played, nwin));

	return 0;

}
